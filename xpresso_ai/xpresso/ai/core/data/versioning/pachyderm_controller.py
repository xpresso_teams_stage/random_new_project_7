from os import path as path_library

import xpresso.ai.core.data.automl.abstract_dataset as dataset_module
from xpresso.ai.core.commons.exceptions.xpr_exceptions import *
from xpresso.ai.core.data.automl import StructuredDataset, UnstructuredDataset
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.versioning.controller_interface \
    import VersionControllerInterface
from xpresso.ai.core.data.versioning.dv_field_name \
    import DataVersioningFieldName as DVFields
from xpresso.ai.core.data.versioning.pachyderm.client \
    import PachydermClient
from xpresso.ai.core.data.versioning.resource_management.branch_manager \
    import BranchManager
from xpresso.ai.core.data.versioning.resource_management.file_manager \
    import DatasetManager
from xpresso.ai.core.data.versioning.resource_management.repo_manager \
    import RepoManager
from xpresso.ai.core.data.versioning.utils \
    import name_validity_check, fetch_file_path_list, format_files_list_for_ui
from xpresso.ai.core.data.versioning.versioning_authenticator \
    import VersioningAuthenticator
from xpresso.ai.core.data.versioning.xpr_dv_branch \
    import XprDataVersioningBranch
from xpresso.ai.core.data.versioning.xpr_dv_commit \
    import XprDataVersioningCommit
from xpresso.ai.core.logging.xpr_log import XprLogger

auth = VersioningAuthenticator()


class PachydermVersionController(VersionControllerInterface):
    """
    Manages repos on pachyderm cluster
    """
    # user input variables
    INPUT_BRANCH_NAME = "branch_name"
    INPUT_COMMIT_ID = "commit_id"
    INPUT_DATASET_NAME = "dataset_name"
    INPUT_DESCRIPTION = "description"
    INPUT_REPO_NAME = "repo_name"
    INPUT_PATH = "path"
    INPUT_DATASET = "dataset"
    LIST = "list"
    PULL = "pull"
    DATA_TYPE = "data_type"
    DATA_TYPE_IS_FILE = "files"
    # Internal Code specific variables
    ACCEPTED_FIELD_NAME_REGEX = r"[\w, -]+$"
    DEFAULT_LIST_DATASET_PATH = "/"
    PICKLE_FILE_EXTENSION = ".pkl"
    PUSH_FILES_MANDATORY_FIELDS = \
        ["repo_name", "branch_name", "data_type", "dataset_name",
         "path", "description", "request_uid"]
    PUSH_DATASET_MANDATORY_FIELDS = ["repo_name", "branch_name", "dataset",
                                     "description", "request_uid"]
    COMMIT_INFO_ID = "commit_id"
    COMMIT_INFO_BRANCH = "branch_name"
    # output variables
    REPO_OUTPUT_NAME = "repo_name"
    FILE_TYPE = "type"
    OUTPUT_COMMIT_FIELD = "commit"
    OUTPUT_COMMIT_ID = "id"
    OUTPUT_BRANCH_HEAD = "head"
    PULL_OUTPUT_TYPE = "output_type"
    OUTPUT_FILE_TYPE = "files"
    PROJECTS = "projects"
    SUPER_USER = "su"
    DV_DATA_SAVE_PATH = "save_at"

    def __init__(self, pachyderm_controller_info, **kwargs):
        """
        constructor class for PachydermVersionController
        """
        super().__init__(**kwargs)
        self.logger = XprLogger()
        try:
            self.pachyderm_client = PachydermClient(
                pachyderm_controller_info["host"],
                pachyderm_controller_info["port"]
            )
            auth.kwargs = kwargs
            auth.update_config()
            self.repo_manager = RepoManager(self.pachyderm_client)
            self.branch_manager = BranchManager(self.pachyderm_client, auth.config)
            self.dataset_manager = DatasetManager(self.pachyderm_client)
            self.commit_manager = self.dataset_manager.commit_manager
        except PachydermOperationException as err:
            raise ValueError(err.message)

    @auth.authenticate_request
    def create_repo(self, **kwargs):
        """
        creates a new repo on pachyderm cluster

        Args:
            kwargs(keyword arguments):
                repo_name(str): name of the repo to be created
                description(str): brief description of this repo
        """
        if self.INPUT_REPO_NAME not in kwargs:
            raise RepoNotProvidedException()
        description = ""
        if self.INPUT_DESCRIPTION in kwargs:
            description = kwargs[self.INPUT_DESCRIPTION]
        self.repo_manager.create(kwargs[self.INPUT_REPO_NAME], description)

    @auth.filter_repo
    def list_repo(self, show_table=False):
        """
        returns the list of all available repos

        :return:
            returns list of repos
        """
        repo_info = self.repo_manager.list()
        if not len(repo_info):
            raise RepoPermissionException("No repos found")
        return self.update_list_response(repo_info, show_table=show_table)

    @auth.authenticate_request
    def create_branch(self, **kwargs):
        """
        creates a new branch in specified repo on pachyderm cluster

        Args:
            kwargs:
                info of the branch to be created
                keys - repo_name, branch_name
                repo_name: name of the repo
                branch_name: name of the branch
        :return:
        """
        branch_object = self.branch_manager.validate_branch_op_input(kwargs)
        repo_name = branch_object.get(self.INPUT_REPO_NAME)
        try:
            self.repo_manager.validate_repo(repo_name)
        except RepoInfoException:
            # lazy creation of repo
            self.logger.debug("Lazy creating a new repo in create_branch")
            self.repo_manager.create(repo_name)
        self.branch_manager.create(branch_object)

    @auth.authenticate_request
    def delete_branch(self, **kwargs):
        """
        Delete a branch in specified repo on pachyderm cluster

        Args:
            kwargs:
                info of the branch to be created
                keys - repo_name, branch_name
                repo_name: name of the repo
                branch_name: name of the branch
        :return:
        """
        branch_object = self.branch_manager.validate_branch_op_input(kwargs)
        self.repo_manager.validate_repo(
            branch_object.get(DVFields.REPO_NAME.value)
        )
        self.branch_manager.delete(branch_object)

    @auth.authenticate_request
    def list_branch(self, filter_info: dict, show_table: bool = False):
        """
        returns a list of branches in specified repo

        Args:
            filter_info: A dict/json with key-value pairs to filter branches
            show_table: this is jupyter notebook specific flag
        """
        branch_object = XprDataVersioningBranch(filter_info)
        # checks if the repo_name is provided and valid
        branch_object.validate_field(DVFields.REPO_NAME.value)
        # checks if branch_type is provided and valid
        branch_object.validate_branch_type()
        branch_info = self.branch_manager.list(branch_object)
        return self.update_list_response(branch_info, show_table=show_table)

    @auth.authenticate_request
    def push_dataset(self, **kwargs):
        """
        pushes file/files into a pachyderm cluster

        :param kwargs:
        :keyword Arguments:
            *repo_name:
                name of the repo
            *branch_name:
                name of the branch
            *dataset:
                AbstractDataset object with info on dataset
            *dataset_name:
                name of dataset that will be pushed to cluster
            *path:
                local path of file or list of files i.e a directory
            *description:
                A brief description on this push
        """
        # data_type field is manually added by controller
        # client before sending request to push_dataset
        self.validate_push_dataset_input(**kwargs)
        if self.DATA_TYPE in kwargs and \
                kwargs[self.DATA_TYPE] == self.DATA_TYPE_IS_FILE:
            # when data_type is files push all files directly to cluster
            path_to_data = kwargs[self.INPUT_PATH]
            dataset_name = kwargs[self.INPUT_DATASET_NAME]
        else:
            # when data_type is not files, then it is assumed to be a dataset
            dataset_name = kwargs[self.INPUT_DATASET].name
            path_to_data = kwargs[self.INPUT_DATASET].save()

        if not path_library.exists(path_to_data):
            raise DatasetPathException(f"path {path_to_data} is invalid")
        elif path_library.isfile(path_to_data):
            dataset_dir = path_library.dirname(path_library.abspath(path_to_data))
        else:
            dataset_dir = path_to_data
        # fetches the path of all the files inside the dataset directory
        file_list, total_file_size = \
            fetch_file_path_list(dataset_dir, dataset_name)
        new_pachyderm_commit_id = self.pachyderm_client.push_dataset(
            kwargs[self.INPUT_REPO_NAME],
            kwargs[self.INPUT_BRANCH_NAME],
            file_list,
            kwargs[self.INPUT_DESCRIPTION]
        )
        commit_object = XprDataVersioningCommit(kwargs)
        commit_object.set(DVFields.DV_COMMIT_ID.value, new_pachyderm_commit_id)
        commit_object.set(DVFields.TOTAL_FILES_IN_COMMIT.value,
                          total_file_size)
        commit_object.set(DVFields.TOTAL_FILES_IN_COMMIT.value,
                          len(file_list))
        branch_object = XprDataVersioningBranch(kwargs)
        if DVFields.BRANCH_TYPE.value not in kwargs:
            branch_object.set(DVFields.BRANCH_TYPE.value,
                              branch_object.default_branch_type)
        new_xpresso_commit_id = \
            self.commit_manager.create(branch_object, commit_object)
        return new_xpresso_commit_id, f"/dataset/{dataset_name}"

    @auth.authenticate_request
    def pull_dataset(self, **kwargs):
        """
        pulls a dataset from pachyderm cluster and load it locally

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
            returns the path of the directory where dataset is saved
        """
        list_output = self.list_dataset(**kwargs)
        # TODO: Add regex check for save at path
        save_at = kwargs[self.DV_DATA_SAVE_PATH] if \
            self.DV_DATA_SAVE_PATH in kwargs else None
        output = self.dataset_manager.pull(
            kwargs[self.INPUT_REPO_NAME], list_output, save_at)
        if self.PULL_OUTPUT_TYPE in kwargs and \
                kwargs[self.PULL_OUTPUT_TYPE] == self.OUTPUT_FILE_TYPE:
            return output
        # TODO: Add abstract resource
        new_dataset = dataset_module.AbstractDataset()
        new_dataset.load(output)
        # Get dataset object of specific type
        new_dataset = self.get_dataset_object(new_dataset)
        new_dataset.load(output)
        return output, new_dataset

    @auth.authenticate_request
    def list_dataset(self, show_table=False, **kwargs):
        """
        list of dataset as per provided information

        Args:
            show_table: flag to specify if the output should be
            beautified for jupyter notebook
        Keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
            returns a dict with file and commit info
        """
        # verifies provided info and returns a dict with request input fields
        self.verify_dataset_input(**kwargs)
        data_info, commit_object = self.complete_dataset_info(**kwargs)
        if data_info[self.INPUT_COMMIT_ID] == '-':
            raise DatasetInfoException("No commits were made to this branch"
                                       " to fetch any dataset")
        list_output = self.dataset_manager.list(
            data_info[self.INPUT_REPO_NAME],
            data_info[self.INPUT_COMMIT_ID],
            data_info[self.INPUT_PATH]
        )
        list_output[self.OUTPUT_COMMIT_FIELD] = commit_object.data
        return self.update_list_response(list_output, show_table=show_table)

    def verify_dataset_input(self, **kwargs):
        """
        verifies input parameters for dataset operations i.e. push, pull & list

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
        """
        if self.INPUT_REPO_NAME not in kwargs:
            raise RepoNotProvidedException
        self.repo_manager.validate_repo(kwargs[self.INPUT_REPO_NAME])

        if self.INPUT_COMMIT_ID not in kwargs and \
                self.INPUT_BRANCH_NAME not in kwargs:
            raise DatasetInfoException(
                "Branch_name is mandatory for dataset level operations."
            )

    def complete_dataset_info(self, **kwargs):
        """
        verifies if the provided info for a dataset is valid or not

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
            returns updated info else throws an exception
            in case of error
        """
        branch_object = XprDataVersioningBranch(kwargs)
        if DVFields.BRANCH_TYPE.value not in kwargs or \
                not kwargs[DVFields.BRANCH_TYPE.value]:
            branch_object.set(
                DVFields.BRANCH_TYPE.value, branch_object.default_branch_type
            )
        if self.INPUT_COMMIT_ID in kwargs and \
                DVFields.BRANCH_NAME.value not in kwargs:
            # Might be deprecated in the future
            # In case pachyderm commit id is provided without branch_name.
            # We need to call pachyderm's inspect commit to fetch commit info
            commit_info = self.commit_manager.inspect(
                kwargs[self.INPUT_REPO_NAME],
                kwargs[self.INPUT_COMMIT_ID]
            )
            # setting branch name fetched from pachyderm
            branch_object.set(DVFields.BRANCH_NAME.value,
                              commit_info[self.COMMIT_INFO_BRANCH])

        # fetching the complete branch information from database
        branch_list = self.branch_manager.list(branch_object, {"_id": False})
        branch_info = branch_list[-1]
        if not len(branch_info[DVFields.BRANCH_COMMITS_KEY.value]):
            raise DatasetInfoException("No commits found in this branch")
        commit_object = self.filter_commit_from_branch(branch_info, kwargs)
        data_info = {
            self.INPUT_REPO_NAME: kwargs[self.INPUT_REPO_NAME],
            self.INPUT_BRANCH_NAME:
                branch_object.get(DVFields.BRANCH_NAME.value),
            self.INPUT_COMMIT_ID:
                commit_object.get(DVFields.DV_COMMIT_ID.value),
            self.INPUT_PATH: self.DEFAULT_LIST_DATASET_PATH
        }
        # If path is not provided default base path is considered
        if self.INPUT_PATH in kwargs:
            data_info[self.INPUT_PATH] = kwargs[self.INPUT_PATH]
        return data_info, commit_object

    @auth.authenticate_request
    def list_commit(self, commit_filter: dict, show_table=False):
        """
        lists all the commits in a repo under a branch

        Args:
            commit_filter: fields to filter out the commits from branches
            show_table: boolean flag to show output as a table on notebook
        Returns:
            list of commits
        """
        # commit is a child component of branch.
        branch_object = XprDataVersioningBranch(commit_filter)
        # commit_filter should provide repo_name & branch_name
        branch_object.validate_mandatory_fields()
        # validate the repo information
        self.repo_manager.validate_repo(
            branch_object.get(DVFields.REPO_NAME.value))
        return self.update_list_response(
            self.commit_manager.list(branch_object), show_table=show_table)

    def validate_push_dataset_input(self, **kwargs):
        """

        :param kwargs:
        :return:
        """
        if self.DATA_TYPE in kwargs and \
                kwargs[self.DATA_TYPE] == self.DATA_TYPE_IS_FILE:
            mandatory_fields = self.PUSH_FILES_MANDATORY_FIELDS
            exempt_fields = [self.INPUT_PATH, "request_uid"]
            # path can have '/'. Hence excluded from this check
        else:
            mandatory_fields = self.PUSH_DATASET_MANDATORY_FIELDS
            exempt_fields = [self.INPUT_DATASET, "request_uid"]
            abstract_dataset = dataset_module.AbstractDataset
            if not isinstance(kwargs[self.INPUT_DATASET], abstract_dataset):
                raise DatasetInfoException("Provided dataset is invalid")

        for field in mandatory_fields:
            if field not in kwargs:
                raise DatasetInfoException(f"'{field}' field not provided")
            elif field not in exempt_fields:
                name_validity_check(field, kwargs[field])
        # for field in kwargs:
        #     if field not in mandatory_fields:
        #         raise DatasetInfoException(f"invalid field '{field}' provided")
        # checks if the repo is valid and does exists
        self.repo_manager.validate_repo(kwargs[self.INPUT_REPO_NAME])
        # check if the branch info is valid or not
        branch_object = XprDataVersioningBranch(kwargs)
        if DVFields.BRANCH_TYPE.value not in kwargs:
            branch_object.set(DVFields.BRANCH_TYPE.value,
                              branch_object.default_branch_type)
        self.branch_manager.list(branch_object)

    def get_dataset_object(self, dataset):
        """
        Return the specific type of dataset depending upon the type
        attribute

        :param dataset:
            Abstract dataset object
        :return:
            Specific dataset object
        """
        try:
            if dataset.type == DatasetType.STRUCTURED:
                return StructuredDataset()
            elif dataset.type == DatasetType.UTEXT:
                return UnstructuredDataset()
            elif dataset.type == DatasetType.DIST_STRUCTURED:
                print("Unsupported for distributed dataset. Try HDFS "
                      "version controller")
                raise DatasetInfoException("Unsupported for distributed dataset. Try HDFS "
                                           "version controller")
        except AttributeError:
            self.logger.error("Dataset type attribute not present")
            raise DatasetInfoException("Dataset type attribute not present")

    def list_dataset_for_ui(self, **kwargs):
        """
        Additional step to modify the output of list_dataset
         specifically for ui api
        """
        list_output = self.list_dataset(**kwargs)
        list_output["dataset"] = \
            format_files_list_for_ui(list_output["dataset"])
        return list_output

    def filter_commit_from_branch(self, branch_info: dict, kwargs: dict):
        """
        filter the commit from the list of commits
        using the input provided by the user

        Args:
            branch_info: info of the branch fetched from db
            kwargs: kwargs provided by user
        """
        commit_list = branch_info[DVFields.BRANCH_COMMITS_KEY.value]
        xpr_commit_id = DVFields.XPRESSO_COMMIT_ID.value
        if xpr_commit_id in kwargs and \
                kwargs[xpr_commit_id] > len(commit_list):
            # xpresso_commit_id value should be less than length of commit_list
            raise DatasetInfoException(
                "Invalid xpresso_commit_id value. Please check & try again."
            )
        if self.INPUT_COMMIT_ID not in kwargs:
            # If commit_id is not provided, fetch the commit info from database
            # If xpresso_commit_id is also not provided,
            # then latest commit is considered
            commit_index = \
                (kwargs[xpr_commit_id] - 1) if xpr_commit_id in kwargs else -1
            commit_object = XprDataVersioningCommit(commit_list[commit_index])
            return commit_object

        commit_id = kwargs[self.INPUT_COMMIT_ID]
        commit_index = None
        for commit_info in commit_list:
            if commit_info[DVFields.DV_COMMIT_ID.value] == commit_id:
                # xpresso_commit_id is assigned by position in the commit list
                # Hence, commit_index is always xpresso_commit_id - 1
                commit_index = commit_info[xpr_commit_id] - 1
                break
        if commit_index is None:
            # If no match from commit_id is found in database
            raise BranchInfoException(
                "Unable to find an entry for the input commit id. "
                "Contact a xpresso developer."
            )
        if xpr_commit_id in kwargs and kwargs[xpr_commit_id] != \
                commit_list[commit_index][xpr_commit_id]:
            # If user provides both xpresso_commit_id & commit_id
            # And if they don't match
            raise BranchInfoException(
                "Input commit_id conflicts with input "
                "xpresso_commit_id. Please check & try again.")
        commit_object = XprDataVersioningCommit(commit_list[commit_index])
        return commit_object
