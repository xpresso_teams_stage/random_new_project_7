from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.exploration.structured_dataset_explorer import \
    StructuredDatasetExplorer
from xpresso.ai.core.data.exploration.unstructured_dataset_explorer import \
    UnstructuredDatasetExplorer
from xpresso.ai.core.data.exploration.semistructured_dataset_explorer import \
    SemiStructuredDatasetExplorer
from xpresso.ai.core.data.exploration.distributed_structured_dataset_explorer \
    import DistributedStructuredDatasetExplorer
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidDatatypeException


class ExplorerFactory:
    """
    Factory class to provide Explorer object for specific dataset type
    """

    @staticmethod
    def get_explorer(dataset):
        """
        This method returns Connector object of a specific datasource
        Args:
            dataset (AbstractDataset): a data set object with specific
                dataset type
        Returns:
            object: Explorer object
        """
        if dataset.type == DatasetType.STRUCTURED:
            ret_object = StructuredDatasetExplorer(dataset)
        elif dataset.type == DatasetType.UTEXT:
            ret_object = UnstructuredDatasetExplorer(dataset)
        elif dataset.type == DatasetType.SEMI_STRUCTURED:
            ret_object = SemiStructuredDatasetExplorer
        elif dataset.type == DatasetType.DIST_STRUCTURED:
            ret_object = DistributedStructuredDatasetExplorer
        else:
            raise InvalidDatatypeException("Provided Data Type : {} not "
                                           "supported".format(dataset.type))
        return ret_object
