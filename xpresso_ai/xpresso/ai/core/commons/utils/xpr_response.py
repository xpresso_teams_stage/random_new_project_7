""" Response Object """


class XprResponse:
    def __init__(self, outcome, error_code, results, request_id=None):
        self.outcome = outcome
        self.error_code = error_code
        self.results = results
        self.request_id = request_id
